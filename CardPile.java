import java.util.Random;

public class CardPile {
	//this is a dynamic array
	private Cards[] cardPile;
	private int pointer;
	
	
	//Generated big card pile
	public CardPile() {
		this.cardPile = new Cards[52];
		this.pointer = 0;
		for(int i = 0; i < 4; i++) {
			addCard(Cards.KING);
			addCard(Cards.QUEEN);
			addCard(Cards.JOKER);
			addCard(Cards.ACE);
			addCard(Cards.TWO);
			addCard(Cards.THREE);
			addCard(Cards.FOUR);
			addCard(Cards.FIVE);
			addCard(Cards.SIX);
			addCard(Cards.SEVEN);
			addCard(Cards.EIGHT);
			addCard(Cards.NINE);
			addCard(Cards.TEN);
		}
		shuffleCards();
	}
	
	public CardPile(int handSize, CardPile pile) {
		this.cardPile = new Cards[handSize];
		this.pointer = 0;
		for(int i = 0; i < 2; i++) {
			addCard(pile.takeCardFromTop());
		}
	}
	
	public int getPointer() {
		return this.pointer;
	}
	
	public Cards getTopCard() {
		return this.cardPile[0];
	}
	
	
	//Definning how the array should be printed
	public String toString() {
		String line = "";
		for(int i = 0; this.cardPile.length > i; i++) {
			if(pointer > i && pointer != i+1) {
				line += this.cardPile[i] + ", ";
			} else if(pointer > i && pointer == i+1) {
				line += this.cardPile[i];
			}
		}
		return line;
	}
	
	//Return the length of Cards list
	public int cardsLength() {
		return this.pointer;
	}
	
	//pile.add(new Card(cardName.ACE, Suit.CLUBS);
	//pile.add(new Card(cardName.KING, Suit.CLUBS);
	//Suits[] values = new Suits{Suits.CLUBS, Suits.HEARTS...}
	//cardName[] names = new cardName{cardName.ACE, cardName.KING....}
	
	//Add a card to the pile or to a player's hand
	public void addCard(Cards aCard) {
		if(pointer >= this.cardPile.length) {
			try {
				throw new RuntimeException("You tried to add too many cards to your hand (the program somehow failed and allowed you to try this");
			}
			catch(RuntimeException maxCardCountReached) {
				System.out.println(maxCardCountReached.getMessage());
			}
		} else {
			this.cardPile[this.pointer] = aCard;
			
			this.pointer++;
		}
	}
	
	//Remove top card from the pile and return it
	public Cards takeCardFromTop() {
		Cards theCard = cardPile[0];
		this.pointer--;
		Cards[] holder = new Cards[52];
		for(int i = 0; i < this.pointer; i++) {
			holder[i] = this.cardPile[i+1];
		}
		this.cardPile = holder;
		
		return theCard;
	}
	
	//shuffle the pile's cards for more randomness
	public void shuffleCards() {
		Random rdr = new Random();
		for(int i = 0; this.pointer > i; i++) {
			int index = rdr.nextInt(pointer);
			Cards holder = cardPile[i];
			this.cardPile[i] = this.cardPile[index];
			this.cardPile[index] = holder;
			
		}
	}
	
	
	//count player card points
	public int countPoints() {
		int total = 0;
		int aceCounter = 0;
		for(int i = 0; i < this.pointer; i++) {
			total += cardPile[i].getCardValue();
			if(this.cardPile[i] == Cards.ACE) {
				aceCounter++;
			}
		}
		
		while(total > 21 && aceCounter > 0) {
				total -= 10;
				aceCounter--;
			}
		return total;
	}
}