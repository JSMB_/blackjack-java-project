public enum WinCondition {
	SP5CARD("5 Card Win"),
	BLACKJACK("BlackNyak"),
	NORMAL21("Regular 21 Win"),
	REGULAR("Regular Win"),
	PUSH("Push"),
	NONE("");
	
	private String stringInterpretation;
	
	private WinCondition(String winName) {
		this.stringInterpretation = winName;
	}
	
	public String toString() {
		return this.stringInterpretation;
	}
}