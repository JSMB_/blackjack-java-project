import java.util.Scanner;

public class BlackNyakApp {
	//This will run the game
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		CardPile pile = new CardPile();
		
		PlayerStat player1 = new PlayerStat(pile, 1);
		PlayerStat player2 = new PlayerStat(pile, 2);
		
		boolean continueGame = true;
		int rounds = 1;
		
		//prints welcoming message and rules
		System.out.println("\n\n\nWelcome to Java BlackNyak!\n\n\n\nRules: \n1. You may only HIT until you have 5 cards (or until you decide to STAND).\n" +
							"2. The dealer will be a player too (AIs are too expensive for the house to afford) \n3. Once you've BUST, you will be unable to do anything until the next round.\n" +
							"4. If you run out of tokens to play, you will be unable to play unless a new game opens.\n" + 
							"5. There must be 3 players in order to play, no more, no less. \n6.Nyau may not bet. \n7. Have fun!\n\n\n");
		System.out.println("Win Conditions: \n1. Natural 21/BlackJack: Your starting hand is a 21. If Nyau has a BlackJack, you don't win (it's a PUSH)\n" +
							"2. 21: You have a total of 21 points in your hand (you will still lose if Nyau has a BlackJack) \n3. Regular win: Your hand has a higher point count than Nyau's(mustn't BUST)\n" + 
							"4. Bust: Your hand goes over 21 points. You lose, even if Nyau busts too\n" + 
							"5. Special 5 Card Win: You have 5 cards in your hand and 21 or less points. You win no matter what. \nAll wins return a different amount of tokens (can be stacked)\n" +
							"If your hand fulfills none of the win conditions above you lose. If Nyau busts, you will still lose if you bust and your bets will not be returned. \n");
		
		//starting loop for the game
		while(continueGame) {
			System.out.println("\n\nSTARTING ROUND: " + rounds);
			
			PlayerStat nyau = new PlayerStat(pile, 3);
			
			boolean ongoingP1 = true;
			boolean ongoingP2 = true;
			boolean p1DD = false;
			boolean p2DD = false;
			Bet p1Bet = Bet.NONE;
			Bet p2Bet = Bet.NONE;
			int p1InitialBalance = player1.getPlayerBalance();
			int p2InitialBalance = player2.getPlayerBalance();
			
			//ifs to determine if the player can play or not
				//Player 1
			boolean invalidInputBet = true;
			if(player1.getPlayerBalance() != 0) {
				while(invalidInputBet) {
					System.out.println("How much will Player 1 be betting? \nOptions: \n1. 10 Tokens \n2. 20 Tokens \n3. 30 Tokens \n4. 40 Tokens \n5. 50 Tokens");
					int betChoice = sc.nextInt();
					if(betChoice == 1) {
						p1Bet = Bet.BET10;
						player1.bet(p1Bet);
						invalidInputBet = false;
					} else if(betChoice == 2) {
						p1Bet = Bet.BET20;
						player1.bet(p1Bet);
						invalidInputBet = false;
					} else if(betChoice == 3) {
						p1Bet = Bet.BET30;
						player1.bet(p1Bet);
						invalidInputBet = false;
					} else if(betChoice == 4) {
						p1Bet = Bet.BET40;
						player1.bet(p1Bet);
						invalidInputBet = false;
					} else if(betChoice == 5) {
						p1Bet = Bet.BET50;
						player1.bet(p1Bet);
						invalidInputBet = false;
					} else {
						try{
							throw new IllegalArgumentException("\nInvalid betting input! Try again:");
						}
						catch(IllegalArgumentException invalidBet) {
							System.out.println(invalidBet.getMessage());
						}
					}
					System.out.println();
				}
			} else {
				ongoingP1 = false;
			}
			
				//Player 2
			if(player2.getPlayerBalance() != 0) {
				invalidInputBet = true;
				int balanceCheck = player2.getPlayerBalance();
				while(invalidInputBet) {
					System.out.println("How much will Player 2 be betting? \nOptions: \n1. 10 Tokens \n2. 20 Tokens \n3. 30 Tokens \n4. 40 Tokens \n5. 50 Tokens");
					int betChoice = sc.nextInt();
					if(betChoice == 1) {
						p2Bet = Bet.BET10;
						player2.bet(p2Bet);
						if(balanceCheck != player2.getPlayerBalance()) {
							invalidInputBet = false;
						}
					} else if(betChoice == 2) {
						p2Bet = Bet.BET20;
						player2.bet(p2Bet);
						if(balanceCheck != player2.getPlayerBalance()) {
							invalidInputBet = false;
						}
					} else if(betChoice == 3) {
						p2Bet = Bet.BET30;
						player2.bet(p2Bet);
						if(balanceCheck != player2.getPlayerBalance()) {
							invalidInputBet = false;
						}
					} else if(betChoice == 4) {
						p2Bet = Bet.BET40;
						player2.bet(p2Bet);
						if(balanceCheck != player2.getPlayerBalance()) {
							invalidInputBet = false;
						}
					} else if(betChoice == 5) {
						p2Bet = Bet.BET50;
						player2.bet(p2Bet);
						if(balanceCheck != player2.getPlayerBalance()) {
							invalidInputBet = false;
						}
					} else {
						try{
							throw new IllegalArgumentException("\nInvalid betting input! Try again:");
						}
						catch(IllegalArgumentException invalidBet) {
							System.out.println(invalidBet.getMessage());
						}
					}
				}
				System.out.println();
			} else {
				ongoingP2 = false;
			}
			
			//the game (player turns until they win or bust if they bet
			int turns = 1;
			while (ongoingP1 || ongoingP2) {
				System.out.println("TURN " + turns + ", START!");
				
				//player 1 turn
				if(ongoingP1 && !(player1.checkIfBlackJack()) && !(player1.checkIfNormal21()) && !(player1.checkIf5Card())) {
					System.out.println("It's Player 1's turn!");
					System.out.println(player1);
					boolean invalidAction = true;
					while(invalidAction) {
						System.out.println("What will you do? \n1. HIT \n2. DOUBLE DOWN \n3. STAND \nEnter the number corresponding to your action:");
						int action = sc.nextInt();
						if(action == 1) {
							System.out.println("\nCard received: " + pile.getTopCard());
							player1.addCard(pile.takeCardFromTop());
							System.out.println("Updated points: " + player1.getPoints());
							invalidAction = false;
							if(player1.checkIfBust()) {
								ongoingP1 = false;
								System.out.println("You have Bust");
							}
						} else if(action == 2) {
							if(turns > 1) {
								try {
									throw new IllegalArgumentException("You may not double down past turn 1! Try again.");
								}
								catch(IllegalArgumentException invalidDD) {
									System.out.println(invalidDD.getMessage());
								}
							} else {
								System.out.println("\nCard received: " + pile.getTopCard());
								player1.addCard(pile.takeCardFromTop());
								System.out.println("Updated points: " + player1.getPoints());
								player1.doubleDown(p1Bet, turns);
								System.out.println("Your bet has increased by " + p1Bet.getBetAmount());
								p1DD = true;
								ongoingP1 = false;
								invalidAction = false;
								if(player1.checkIfBust()) {
									System.out.println("You have Bust");
								}
							}
						} else if(action == 3) {
							ongoingP1 = false;
							invalidAction = false;
						} else {
							try {
								throw new IllegalArgumentException("You've input an invalid action number! Try again.\n");
							}
							catch(IllegalArgumentException invalidPlayerAction) {
								System.out.println(invalidPlayerAction.getMessage());
							}
						}
						
						if(player1.checkIfNormal21()) {
							System.out.println("Player 1 got a normal 21! \n\n\n-----------Player 1 Status-----------\n" + player1 + "\n");
							ongoingP1 = false;
						} else if(player1.checkIf5Card()) {
							System.out.println("Player 1 got a 5 card win! \n\n\n-----------Player 1 Status-----------\n" + player1 + "\n");
							ongoingP1 = false;
						}
					}
					System.out.println("\n\nPlayer 1 turn end.\n");
					System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n");
				} else if(ongoingP1 && player1.checkIfBlackJack()) {
					System.out.println("\n\nPlayer 1 got a BlackNyak! \n\n\n-----------Player 1 Status-----------\n" + player1 + "\n");
					ongoingP1 = false;
				} else if(ongoingP1 && player1.checkIfNormal21()) {
					System.out.println("\n\nPlayer 1 got a normal 21! \n\n\n-----------Player 1 Status-----------\n" + player1 + "\n");
					ongoingP1 = false;
				} else if(ongoingP1 && player1.checkIf5Card()) {
					System.out.println("\n\nPlayer 1 got a 5 card win! \n\n\n-----------Player 1 Status-----------\n" + player1 + "\n");
					ongoingP1 = false;
				}
				
				
				//player 2 turn
				if(ongoingP2 && !(player2.checkIfBlackJack()) && !(player2.checkIfNormal21() && !(player2.checkIf5Card()))) {
					System.out.println("It's Player 2's turn!");
					System.out.println(player2);
					boolean invalidAction = true;
					while(invalidAction) {
						System.out.println("What will you do? \n1. HIT \n2. DOUBLE DOWN \n3. STAND \nEnter the number corresponding to your action:");
						int action = sc.nextInt();
						if(action == 1) {
							System.out.println("\nCard received: " + pile.getTopCard());
							player2.addCard(pile.takeCardFromTop());
							System.out.println("Updated points: " + player2.getPoints());
							invalidAction = false;
							if(player2.checkIfBust()) {
								ongoingP2 = false;
								System.out.println("You have Bust");
							}
						} else if(action == 2) {
							if(turns > 1) {
								try {
									throw new IllegalArgumentException("You may not double down past turn 1! Try again.");
								}
								catch(IllegalArgumentException invalidDD) {
									System.out.println(invalidDD.getMessage());
								}
							} else {
								System.out.println("\nCard received: " + pile.getTopCard());
								player2.addCard(pile.takeCardFromTop());
								System.out.println("Updated points: " + player2.getPoints());
								player2.doubleDown(p1Bet, turns);
								System.out.println("Your bet has increased by " + p2Bet.getBetAmount());
								p2DD = true;
								ongoingP2 = false;
								invalidAction = false;
								if(player2.checkIfBust()) {
									System.out.println("You have Bust");
								}
							}
						} else if(action == 3) {
							ongoingP2 = false;
							invalidAction = false;
						} else {
							try {
								throw new IllegalArgumentException("\nYou've input an invalid action number! Try again.\n");
							}
							catch(IllegalArgumentException invalidPlayerAction) {
								System.out.println(invalidPlayerAction.getMessage());
							}
						}
						
						if(player2.checkIfNormal21()) {
							System.out.println("\nPlayer 2 got a normal 21! \n-----------Player 2 Status-----------\n" + player2 + "\n");
							ongoingP2 = false;
						} else if(player2.checkIf5Card()) {
							System.out.println("Player 2 got a 5 card win! \n\n\n-----------Player 2 Status-----------\n" + player2 + "\n");
							ongoingP2 = false;
						}
					}
					System.out.println("\n\nPlayer 2 turn end.\n");
					System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n");
				} else if(ongoingP2 && player2.checkIfBlackJack()) {
					System.out.println("\n\nPlayer 2 got a BlackNyak! \n-----------Player 2 Status-----------\n" + player2 + "\n");
					ongoingP2 = false;
				} else if(ongoingP2 && player2.checkIfNormal21()) {
					System.out.println("\n\nPlayer 2 got a normal 21! \n-----------Player 2 Status-----------\n" + player2 + "\n");
					ongoingP2 = false;
				} else if(ongoingP2 && player2.checkIf5Card()) {
					System.out.println("\n\nPlayer 2 got a 5 card win! \n\n\n-----------Player 2 Status-----------\n" + player2 + "\n");
					ongoingP2 = false;
				}
				turns++;
			}
			
			
			//Nyau's turn
			boolean ongoingNyau = true;
			while(ongoingNyau) {
				if(ongoingNyau && !(nyau.checkIfBlackJack()) && !(nyau.checkIfNormal21()) && !(nyau.checkIf5Card())) {
					System.out.println("It's Nyau's turn!");
					System.out.println("Nyau's hand: " + nyau.getPlayerHand() + "\nCurrent points: " + nyau.getPoints());
					boolean invalidAction = true;
					while(invalidAction) {
						System.out.println("What will you do? \n1. HIT \n2. STAND \nEnter the number corresponding to your action:");
						int action = sc.nextInt();
						if(action == 1) {
							System.out.println("\nCard received: " + pile.getTopCard());
							nyau.addCard(pile.takeCardFromTop());
							System.out.println("Updated points: " + nyau.getPoints());
							invalidAction = false;
							if(nyau.checkIfBust()) {
								ongoingNyau = false;
								System.out.println("You have Bust");
							}
						} else if(action == 2) {
							ongoingNyau = false;
							invalidAction = false;
						} else {
							try {
								throw new IllegalArgumentException("\nYou've input an invalid action number! Try again.\n");
							}
							catch(IllegalArgumentException invalidPlayerAction) {
								System.out.println(invalidPlayerAction.getMessage());
							}
						}
						
						if(nyau.checkIfNormal21()) {
							System.out.println("\nNyau got a normal 21! \n-----------Nyau Status-----------\n" + nyau + "\n");
							ongoingNyau = false;
						}
						else if(nyau.checkIf5Card()) {
							System.out.println("Nyau got a 5 card win! \n\n\n-----------Nyau Status-----------\n" + nyau + "\n");
							ongoingNyau = false;
						}
					}
					System.out.println("\n\nNyau's turn end.\n\n");
					System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n");
				} else if(ongoingNyau && nyau.checkIfBlackJack()) {
					System.out.println("\n\nNyau got a BlackNyak! \n-----------Nyau Status-----------\n" + nyau + "\n");
					ongoingNyau = false;
				} else if(ongoingNyau && nyau.checkIfNormal21()) {
					System.out.println("\n\nNyau got a normal 21! \n-----------Nyau Status-----------\n" + nyau + "\n");
					ongoingNyau = false;
				}
				else if(ongoingNyau && nyau.checkIf5Card()) {
					System.out.println("\n\nNyau got a 5 card win! \n\n\n-----------Nyau Status-----------\n" + nyau + "\n");
					ongoingNyau = false;
				}
			}
			
			
			
			//Increasing bet if double down
			int p1FinalBalance = player1.getPlayerBalance();
			int p2FinalBalance = player2.getPlayerBalance();
			int p1Return = p1Bet.getBetAmount();
			int p2Return = p2Bet.getBetAmount();
			if(p1DD) {
				p1Return = (p1FinalBalance - p1InitialBalance) * -1;
			}
			if(p2DD) {
				p2Return = (p2FinalBalance - p2InitialBalance) * -1;
			}
			
			//Determine who wins and the returns from each bet
			player1.winner(nyau, p1Return);

			player2.winner(nyau, p2Return);
			
			System.out.println("\n\n\n");
			
			//Continue game?
			pile = new CardPile();
			int playersContinuing = 0;
			if(player1.getPlayerBalance() > 0 || player2.getPlayerBalance() > 0) {
				if(player1.getPlayerBalance() > 0) {
					String oneMoreGame = "";
					System.out.println("One more game? \n(Players with no tokens left may not rejoin until program termination) \n*WARNING*Players who want to leave will immediately drop to 0 tokens," 
											+ "and they may not rejoin until program termination \nPlayer 1 - Continue? (Y/N)");
					do {
						oneMoreGame = sc.next();
						if(oneMoreGame.equalsIgnoreCase("Y")) {
							player1.resetPlayerHand(pile);
							rounds++;
							playersContinuing++;
						} else if(oneMoreGame.equalsIgnoreCase("N")) {
							player1.dropBalance();
						} else {
							try {
								throw new IllegalArgumentException("Invalid answer! Please try again:");
							}
							catch(IllegalArgumentException invalidContinue) {
								System.out.println(invalidContinue.getMessage());
							}
						}
					} while(!(oneMoreGame.equalsIgnoreCase("Y")));
				}
				
				System.out.println("\n");
				
				if(player2.getPlayerBalance() > 0) {
					
					String oneMoreGame = "";
					System.out.println("One more game? \n(Players with no tokens left may not rejoin until program termination) \n*WARNING*Players who want to leave will immediately drop to 0 tokens," 
											+ "and they may not rejoin until program termination \nPlayer 2 - Continue? (Y/N)");
					do {
						oneMoreGame = sc.next();
						if(oneMoreGame.equalsIgnoreCase("Y")) {
							player2.resetPlayerHand(pile);
							if(playersContinuing == 0) {
								rounds++;
							}
							playersContinuing++;
						} else if(oneMoreGame.equalsIgnoreCase("N")) {
							player2.dropBalance();
						} else {
							try {
								throw new IllegalArgumentException("Invalid answer! Please try again:");
							}
							catch(IllegalArgumentException invalidContinue) {
								System.out.println(invalidContinue.getMessage());
							}
						}
					} while(!(oneMoreGame.equalsIgnoreCase("Y")) && !(oneMoreGame.equalsIgnoreCase("N")));
				}
				
				if(playersContinuing == 0) {
					continueGame = false;
				}
			} else {
				System.out.println("\n\nNone of the players have enough tokens to continue. Forced exit");
				continueGame = false;
			}
		}
	}
}
	
	//Methods used for testing (some may be outdated due to varying changes on the different versions of my code)


	//[WARNING] I am only keeping this method here as a comment because it helped me figure out how to determine the tie outside of Class>CardPile
	/*public static boolean checkIfTie(PlayerStat player, PlayerStat dealer) {
		CardPile p1 = player.getPlayerHand();
		CardPile d = dealer.getPlayerHand();
		
		if(p1.countPoints() == d.countPoints()) {
			return true;
		}
		return false;
	}*/
	
	
	//testing section
	
	//testing PlayerStat class
	/*public static void main(String[] args) {
		PlayerStat player1 = new PlayerStat();
		
		System.out.println(player1);
		
		//Testing the betting amounts
		player1.bet(Bet.BET10);
		System.out.println(player1);
		
		player1.bet(Bet.BET20);
		System.out.println(player1);
		
		player1.bet(Bet.BET30);
		System.out.println(player1);
		
		player1.bet(Bet.BET40);
		System.out.println(player1);
		
		player1.bet(Bet.BET50);
		System.out.println(player1);
		
		//Testing limiting betting
		player1.bet(Bet.BET50);
		System.out.println(player1);
		player1.bet(Bet.BET50);
		System.out.println(player1);
		player1.bet(Bet.BET10);
		System.out.println(player1);
	}*/
	
	//testing CardPile class
	/*public static void main(String[] args) {
		//testing the cardPile stuff + the shuffling method
		CardPile theStack = new CardPile(10);
		
		theStack.addCard(Cards.ACE);
		theStack.addCard(Cards.QUEEN);
		theStack.addCard(Cards.KING);
		theStack.addCard(Cards.ACE);
		theStack.addCard(Cards.QUEEN);
		theStack.addCard(Cards.KING);
		theStack.addCard(Cards.ACE);
		theStack.addCard(Cards.QUEEN);
		theStack.addCard(Cards.KING);
		theStack.addCard(Cards.QUEEN);
		
		System.out.println(theStack);
		
		theStack.shuffleCards();
		
		System.out.println(theStack);
		
		//TakeCardFromTop() testing
			//pt1
		/*CardPile theStack = new CardPile();
		Cards ACE = Cards.ACE;
		Cards QUEEN = Cards.QUEEN;
		Cards KING = Cards.KING;
		
		theStack.addCard(ACE);
		theStack.addCard(QUEEN);
		theStack.addCard(KING);
		
		System.out.println(theStack);
		
		theStack.takeCardFromTop();
		
		System.out.println(theStack);*/
		
			//pt2
		/*CardPile hando = new CardPile(5);
		
		hando.addCard(Cards.ACE);
		hando.addCard(Cards.QUEEN);
		hando.addCard(Cards.KING);
		
		System.out.println(hando);
		
		hando.takeCardFromTop();
		
		System.out.println(hando);*/
		

		//countPoints()+checkIfBust() Testing
		/*CardPile theStack = new CardPile();
		
			//pt1
		theStack.addCard(Cards.ACE);
		theStack.addCard(Cards.QUEEN);
		theStack.addCard(Cards.KING);
		
		System.out.println(theStack);
		
		System.out.println(theStack.countPoints());
		System.out.println(theStack.checkIfBust());
		
			//pt2
		theStack.addCard(Cards.TEN);
		theStack.addCard(Cards.QUEEN);
		theStack.addCard(Cards.KING);
		
		System.out.println(theStack);
		
		System.out.println(theStack.countPoints());
		System.out.println(theStack.checkIfBust());*/
		
		
		//checkIfBlackJack() & other win conditions Testing
		/*CardPile theStack = new CardPile();
		
			//pt1
		theStack.addCard(Cards.ACE);
		theStack.addCard(Cards.QUEEN);
		
		System.out.println(theStack);
		
		System.out.println(theStack.checkIfBlackJack());
		
		//pt2
		theStack.addCard(Cards.KING);
		theStack.addCard(Cards.ACE);
		theStack.addCard(Cards.QUEEN);
		
		System.out.println(theStack);
		
		System.out.println(theStack.checkIfBlackJack());*/
		
		
		//Win types testing
			//checkIf5Card()
		/*CardPile h1 = new CardPile(5);
		h1.addCard(Cards.THREE);
		h1.addCard(Cards.THREE);
		h1.addCard(Cards.THREE);
		h1.addCard(Cards.THREE);
		h1.addCard(Cards.KING);
		System.out.println(h1.checkIf5Card());*/
		
		
			//checkIfNormal21()
		/*CardPile h2 = new CardPile(5);
		h2.addCard(Cards.KING);
		h2.addCard(Cards.ACE);
		h2.addCard(Cards.QUEEN);
		System.out.println(h2.checkIfNormal21());*/
		
		
			//checkIfNormal21() + checkIf5Card()
		/*CardPile h3 = new CardPile(5);
		h3.addCard(Cards.KING);
		h3.addCard(Cards.ACE);
		h3.addCard(Cards.FIVE);
		h3.addCard(Cards.ACE);
		h3.addCard(Cards.FOUR);
		boolean check = false;
		if(h3.checkIfNormal21() && h3.checkIf5Card()) {
			check = true;
		}
		System.out.println(check);*/
		
		//Testing checkIfTie()
		/*PlayerStat player = new PlayerStat();
		PlayerStat dealer = new PlayerStat();
		
		player.addCard(Cards.KING);
		player.addCard(Cards.ACE);
		
		dealer.addCard(Cards.KING);
		dealer.addCard(Cards.KING);
		
		System.out.println(checkIfTie(player, dealer));
		System.out.println(player.checkIfPush(dealer));
	}*/