public enum Bet {
	BET10(10),
	BET20(20),
	BET30(30),
	BET40(40),
	BET50(50),
	NONE(0);
	
	private int tokensBet;
	
	private Bet(int tokens) {
		this.tokensBet = tokens;
	}
	
	public int getBetAmount() {
		return this.tokensBet;
	}
}