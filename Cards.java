public enum Cards {
	ACE(11),
	KING(10),
	QUEEN(10),
	JOKER(10),
	TWO(2),
	THREE(3),
	FOUR(4),
	FIVE(5),
	SIX(6),
	SEVEN(7),
	EIGHT(8),
	NINE(9),
	TEN(10);
	
	private int points;
	
	private Cards(int points) {
		this.points = points;
	}
	
	public int getCardValue() {
		return this.points;
	}
}