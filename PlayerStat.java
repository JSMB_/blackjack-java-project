import java.util.Scanner;

public class PlayerStat {
	private CardPile hand;
	private int balance;
	private int playerPoints;
	private int playerID;
	
	public PlayerStat(CardPile pile, int playerID) {
		this.hand = new CardPile(5, pile);
		this.balance = 250;
		this.playerPoints = this.hand.countPoints();
		this.playerID = playerID;
	}
	
	public String toString() {
		return "Current player's hand: " + this.hand.toString() + "\nPoints: " + this.playerPoints + "\nCurrent player's balance: " + this.balance;
	}
	
	//this method will reset the player's hand on each new round. I've implemented this so that I don't have to initiate the player object inside the loop (this would've meant I would've also reset the token balance
	public void resetPlayerHand(CardPile pile) {
		this.hand = new CardPile(5, pile);
		this.playerPoints = this.hand.countPoints();
	}
	
	public CardPile getPlayerHand() {
		return this.hand;
	}
	
	public int getPlayerBalance() {
		return this.balance;
	}
	
	public int getPoints() {
		return this.playerPoints;
	}
	
	public void doubleDown(Bet amountBet, int turn) {
		if(turn == 1) {
			bet(amountBet);
		}
	}
	
	//this method will immediately drop the player's balance to 0
	public void dropBalance() {
		this.balance -= this.balance;
	}
	
	public int getPointer() {
		return this.hand.getPointer();
	}
	
	public void bet(Bet amountBet) {
		int tokensBet = amountBet.getBetAmount();
		if(this.balance < tokensBet) {
			try {
				throw new RuntimeException("Not enough tokens to bet.");
			}
			catch(RuntimeException notEnoughTokens) {
				System.out.println(notEnoughTokens.getMessage());
			}
		} else {
			this.balance -= tokensBet;
		}
	}
	
	//30 bonus per special card (if added)
	public int betReturn(WinCondition win, int returnTokens) {
		int tokens = 0;
		if(win == WinCondition.SP5CARD) {
			tokens += returnTokens * 2 + 250;
			this.balance += tokens;
		}
		if(win == WinCondition.BLACKJACK) {
			tokens += returnTokens * 2 + 150;
			this.balance += tokens;
		}
		if(win == WinCondition.NORMAL21) {
			tokens += returnTokens * 2 + 100;
			this.balance += tokens;
		}
		if(win == WinCondition.REGULAR) {
			tokens += returnTokens * 2;
			this.balance += tokens;
		}
		if(win == WinCondition.PUSH) {
			tokens += returnTokens;
			this.balance += returnTokens;
		}
		return tokens;
	}
	
	//basic methods
	public void addCard(Cards aCard) {
		this.hand.addCard(aCard);
		this.playerPoints = this.hand.countPoints();
	}
	
	//Win methods (I had already written them on my CardPile class and transporting them over here was a huge hassle, so doing what I did below became an easier solution + I couldn't really access private pointer in here)
	//checker methods
	public boolean checkIfBust() {
		if(this.playerPoints > 21) {
			return true;
		}
		return false;
	}
	
	public boolean checkIfBlackJack() {
		if(this.hand.cardsLength() == 2 && this.playerPoints == 21) {
				return true;
			}
		return false;
	}
	
	public boolean checkIfNormal21() {
		if(this.hand.cardsLength() > 2 && this.playerPoints == 21) {
				return true;
			}
		return false;
	}
	public boolean checkIf5Card() {
		if(this.hand.cardsLength() == 5 && !(checkIfBust())) {
			return true;
		}
		return false;
	}
	
	public boolean checkIfPush(PlayerStat dealer) {
		if(getPoints() == dealer.getPoints() || checkIfBlackJack() == dealer.checkIfBlackJack() || checkIfNormal21() == dealer.checkIfNormal21()) {
			return true;
		}
		return false;
	}
	
	public boolean checkIfWin() {
		if(checkIfNormal21() || checkIfBlackJack() || checkIf5Card() && !(checkIfBust())) {
			return true;
		}
		return false;
	}
	
	//winner winner chicken dinner (check if player win, return bets, and print win message)
	public void winner(PlayerStat nyau, int playerReturn) {
		//nyau is the dealer
		WinCondition win = WinCondition.NONE;
		
		//typing the nyau PlayerStat + method for points is long, so I'm making it shorter like this
		int nyauPoints = nyau.getPoints();
			if(!(checkIfBust())) {
				int mixedWins = 0;
				//double BlackNyak + (optional) 5 CARDS
				if(checkIfBlackJack() && nyau.checkIfBlackJack()) {
					win = WinCondition.PUSH;
					System.out.println("Player " + this.playerID + " and Nyau both got a " + WinCondition.BLACKJACK + "! It is a " + win + 
										" Neither win! \n Returned " + betReturn(win, playerReturn) + "Tokens to the player");
				//player blackNyak
				} else if(checkIfBlackJack() && (!(nyau.checkIfBlackJack()) || !(nyau.checkIfNormal21()) || nyau.checkIfNormal21())) {
					win = WinCondition.BLACKJACK;
					System.out.println("Player " + this.playerID + " BlackNyak! \nReturned " + betReturn(win, playerReturn) + " Tokens to the player!");
				//PUSH
				} else if(getPoints() == nyauPoints && !(checkIf5Card()) && !(nyau.checkIf5Card())) {
					win = WinCondition.PUSH;
					System.out.println("Player " + this.playerID + " and Nyau have the same amount of points! It as a " + win + 
										", therefore neither win! \nReturned " + betReturn(win, playerReturn) + " Tokens to the player!");
				//player and nya same points, but player has 5 CARDS
				} else if(checkIf5Card()) { //Even if nyau gets 5 cards too and has the same points, the player will still take priority and win.
					win = WinCondition.SP5CARD; //^^^this is fully intentional
					if(checkIfNormal21()) {
						mixedWins += betReturn(win, playerReturn);
						WinCondition win2 = WinCondition.NORMAL21;
						mixedWins += betReturn(win2, playerReturn);
						System.out.println("Player " + this.playerID + " has a " + win + " and a " + win2 + ". Player 1 automatically wins! \nReturned " 
											+ mixedWins + " Tokens to the player!");
					} else {
						System.out.println("Player " + this.playerID + " has " + win + ". Player " + playerID + " automatically wins! \nReturned " 
											+ betReturn(win, playerReturn) + " Tokens to the player!");
					}
				//nyau 5CARD
				} else if(nyau.checkIf5Card() && !(checkIf5Card())) {
					System.out.println("Nyau has 5 cards and the player doesn't. Player " + this.playerID + " automatically loses! \n No Tokens will be returned to the player!");
				} else if(checkIfNormal21() && nyau.checkIfNormal21()) {
					win = WinCondition.PUSH;
					System.out.println("Player " + this.playerID + " and Nyau both got a " + WinCondition.NORMAL21 + "! It is a " + win 
										+ ", therefore neither win! \nReturned " + betReturn(win, playerReturn) + "Tokens to the player");
				} else if(checkIfNormal21() && !(nyau.checkIfNormal21())) {
					win = WinCondition.NORMAL21;
					System.out.println("Player " + this.playerID + " has 21 points and Nyau doesn't! \nReturned " + betReturn(win, playerReturn) 
										+ " Tokens to the player!");
				//Player REGULAR
				} else if(getPoints() > nyauPoints) {
					win = WinCondition.REGULAR;
					System.out.println("Player " + this.playerID + " has more points than nyau. Player" + this.playerID + "wins! \nReturned " + betReturn(win, playerReturn) + " Tokens to the player!");
				} else if (nyau.checkIfBust() && !(checkIfBust())) {
					win = WinCondition.REGULAR;
					System.out.println("Nyau Bust. Player " + this.playerID + " wins! \nReturned " + betReturn(win, playerReturn) + " Tokens to the player!");
				} else {
					System.out.println("Player " + this.playerID + " has less points than Nyau. No tokens returned to the player!");
				}
			} else {
				System.out.println("Player "+ this.playerID + " Bust. No Tokens returned to the player!");
			}
	}
}